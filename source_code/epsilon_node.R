#' ####################################################################################################################
#' ## This function returns the epsilon value for given variable/node of the network (GE, CNV, METH)                 ##
#' ####################################################################################################################
#' @export
epsilon_node <- function(feature, net, B_prior_mat)
{
  parent <- names(net[,feature])[net[,feature]==1]
  noparent <- setdiff(rownames(net),parent)
  return(sum(1-B_prior_mat[parent,feature]) + sum(B_prior_mat[noparent,feature]))
}